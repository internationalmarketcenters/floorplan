(function(angular) {
  'use strict';

  angular.module('AMC.FloorPlan')
    .factory('HammerDeltaTracker', [HammerDeltaTrackerProducer]);

  function HammerDeltaTrackerProducer() {
    /** Helper to track pinch/pan deltas. */
    class HammerDeltaTracker {
      /**
       * Init.
       */
      constructor() {
        this._lastCenter = null;
      }

      /**
       * Get the x-y change since the last event.
       */
      getDelta(e) {
        // Finger up.  Reset last placement.
        if (e.isFinal) {
          this._lastCenter = null;
          return null;
        }

        // Drag started (finger down).  Store center.
        if (!this._lastCenter) {
          this._lastCenter = e.center;
          return null;
        }

        // Drag.  Compute delta and store center.
        const delta = {
          x: this._lastCenter.x - e.center.x,
          y: this._lastCenter.y - e.center.y
        };

        this._lastCenter = e.center;

        return delta;
      }
    }

    return HammerDeltaTracker;
  }
})(window.angular);

