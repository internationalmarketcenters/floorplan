module.exports = function(grunt) {
  'use strict';

  const opts  = {
    ignore: [
      'node_modules/**',
      'AMC.FloorPlan.js',
      'grunt/**/*.js',
      'Gruntfile.js',
      'build/**/*',
    ]
  };
  const glob  = require('glob');
  const files = [
    'node_modules/angular/angular.min.js',
    'node_modules/angular-mocks/angular-mocks.js',
    'AMC.FloorPlan.js'
  ].concat(glob.sync('**/*.js', opts));
  
  const karma = {
    options: {
      frameworks: ['jasmine'],
      port:       8765,
      files:      files,
    },
    interactive: {},
  };

  grunt.loadNpmTasks('grunt-karma');

  return karma;
};

