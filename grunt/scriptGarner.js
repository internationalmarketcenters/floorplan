module.exports = function() {
  'use strict';

  const glob     = require('glob');
  const globOpts = {cwd: __dirname + '/../'};
  const base     = __dirname + '/../';
  const files    = [base + 'AMC.FloorPlan.js']
    .concat(glob.sync('**/*.js', globOpts).filter(script => {
      return !script.match(/node_modules/) &&
             !script.match(/AMC.FloorPlan.js/) &&
             !script.match(/build/) &&
             !script.match(/grunt/i) &&
             !script.match(/Spec.js/);
    }));

  console.dir(files);
  return files;
};

