module.exports = function(grunt, scripts) {
  'use strict';

  const jshint = {
    options: {
      strict:    true,
      eqeqeq:    true,
      indent:    2,
      quotmark:  'single',
      undef:     true,
      unused:    true,
      esnext:    true
    },

    app: {
      options: {
        globals: {
          angular: true,
          window:  true
        },
        sub: true
      },
      files: {
        src: scripts
      }
    }
  };

  grunt.loadNpmTasks('grunt-contrib-jshint');

  return jshint;
};

