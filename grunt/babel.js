module.exports = function(grunt, buildFile) {
  'use strict';

  const babel = {
    options: {
      presets: ['@babel/preset-env']
    },
    dist: {
      files: [{ 
        expand: true, 
        src: buildFile
      }]
    }
  };

  grunt.loadNpmTasks('grunt-babel');

  return babel;
};

