(function(angular) {
  'use strict';

  angular
    .module('AMC.FloorPlan', [
      'AMC.Graphics',
      'angular-gestures',
    ])
    .config(['hammerDefaultOptsProvider', function(hammerDefaultOptsProvider) {
      hammerDefaultOptsProvider
        .set({
          recognizers: [
            [window.Hammer.Pan, {time: 0}],
            [window.Hammer.Pinch, {time: 0}]
          ]});
    }]);
})(window.angular);

