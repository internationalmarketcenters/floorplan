(function(angular) {
  'use strict';

  angular.module('AMC.FloorPlan')
    .directive('amcSvgColor', [
      '$parse',
      'colorHelper',
      AmcSvgColorProducer
    ]);

  function AmcSvgColorProducer($parse, colorHelper) {
    return {
      restrict: 'A',
      link: function(scope, ele, attrs) {
        const svgEle   = ele[0];
        const getColor = $parse(attrs.amcSvgColor).bind(this, scope);
        let   hovering = false;

        function updateColor() {
          let color = getColor();

          if (hovering)
            color = colorHelper.lighten(color, 60);

          svgEle.setAttributeNS(null, 'fill', color);
        }

        // Color is darkened on hover.
        ele.on('mouseover', () => {
          hovering = true;
          updateColor();
          scope.$apply();
        });

        ele.on('mouseout', () => {
          hovering = false;
          updateColor();
          scope.$apply();
        });

        scope.$watch(() => getColor(), updateColor);
      }
    };
  }
})(window.angular);

