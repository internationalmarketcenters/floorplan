(function(angular) {
  'use strict';

  angular.module('AMC.FloorPlan')
    .directive('amcSvgPolygon', [
      '$parse',
      'matrixHelper',
      AmcSvgPolygonProducer
    ]);

  function AmcSvgPolygonProducer($parse, matrixHelper) {
    return {
      restrict: 'A',
      link: function(scope, ele, attrs) {
        const poly     = ele[0];
        const getVerts = $parse(attrs.amcSvgPolygon).bind(this, scope);
        const verts    = getVerts();

        poly.setAttributeNS(null, 'points', matrixHelper.verticesToString(verts));
      }
    };
  }
})(window.angular);

