(function(angular) {
  'use strict';

  angular.module('AMC.FloorPlan')
    .component('amcBoothDetails', {
      template     : `
        <div
          class="amc-booth-details position-fixed border pad-10 bg-white shadow"
          ng-if="vm.booth"
          amc-position-with-mouse>

          <div class="location">
            <b>Mart Location:</b>
            <span ng-bind="::vm.booth.bthTitle"></span>
          </div>

          <div class="dimensions">
            <b>Dimensions:</b>
            <span ng-bind="::vm.getDimensions()"></span>
          </div>

          <div class="sqft">
            <b>Square Feet:</b>
            <span ng-bind="::vm.booth.bthSquareFeet"></span>
          </div>

          <div class="corners" ng-show="::vm.booth.corners">
            <b>Corners:</b>
            <span ng-bind="::vm.booth.corners"></span>
          </div>

          <div class="cost" ng-show="::vm.booth.bthPrice">
            <b>Cost:</b>
            <span ng-bind="::vm.booth.bthPrice | currency"></span>
          </div>

          <div class="exhibitors" ng-show="::vm.booth.exhibitors.length">
            <b>Exhibitors:</b>
            <div ng-repeat="ex in vm.booth.exhibitors">
              <span class="pad-l-10" ng-bind="::vm.getExhibName(ex)"></span>
            </div>
          </div>

          <div class="notes" ng-show="::vm.booth.notes">
            <b>Notes:</b>
            <div
              class="pad-l-10"
              style="max-width: 250px;"
              ng-bind="::vm.booth.notes">
            </div>
          </div>
        </div>
      `,
      controller   : [
        AmcBoothDetailsCtrl
      ],
      bindings     : {
        booth : '<'
      },
      controllerAs : 'vm'
    });

  function AmcBoothDetailsCtrl() {
    const vm = this;

    vm.getDimensions = () => `${vm.booth.bthWidth}' x ${vm.booth.bthDepth}'`;
    // Legacy: Booth Area API returns exhibName.
    vm.getExhibName  = exhib => exhib.showroomName || exhib.exhibName;
  }
})(window.angular);

