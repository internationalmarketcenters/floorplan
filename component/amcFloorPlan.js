(function(angular) {
  'use strict';

  angular.module('AMC.FloorPlan')
    .component('amcFloorPlan', {
      template: `
        <svg
          xmlns="http://www.w3.org/2000/svg"
          version="1.1"
          amc-svg-size="::vm.mapSize"
          amc-on-scroll="vm.zoom(e)"
          hm-drag="vm.drag($event)"
          hm-pinch="vm.pinch($event)">

          <image
            amc-map-graphic="::vm.mapGraphic"
            amc-svg-size="::vm.mapSize"
            amc-transform="vm.viewingTransformer.getVTM()">
          </image>

          <g
            amc-transform="vm.calibratedVTM">
            <polygon
              class="border"
              ng-repeat="booth in vm.booths"
              amc-svg-polygon="booth.vertices"
              amc-svg-color="booth.color"
              ng-click="vm.onBoothClick({booth})"
              ng-mouseover="vm.onBoothMouseover({booth})"
              ng-mouseout="vm.onBoothMouseout({booth})">
            </polygon>
          </g>
        </svg>
      `,
      controller : [
        'ViewingTransformer',
        'matrixHelper',
        'HammerDeltaTracker',
        AmcFloorPlanCtrl
      ],
      bindings: {
        boothArea        : '<',
        booths           : '<',
        calTransforms    : '<calibrationTransforms',
        mapGraphic       : '<',
        onBoothClick     : '&',
        onBoothMouseover : '&',
        onBoothMouseout  : '&',
      },
      controllerAs : 'vm'
    });

  function AmcFloorPlanCtrl(ViewingTransformer, matrixHelper, HammerDeltaTracker) {
    const vm = this;

    let calibrationMatrix = null;
    const panDeltaTracker = new HammerDeltaTracker();

    vm.$onInit            = initFloorPlan;
    vm.mapSize            = null;
    vm.viewingTransformer = null;
    vm.calibratedVTM      = null;
    vm.zoom               = zoom;
    vm.pan                = pan;
    vm.drag               = drag;
    vm.pinch              = pinch;

    function initFloorPlan() {
      ['boothArea', 'booths', 'calTransforms', 'mapGraphic']
        .forEach(binding => {
          if (!vm[binding])
            throw new Error(`amcFloorPlan.js: The "${binding}" binding is required but not present.`);
        });

      // The amcSvgSize directive (from AMC.Graphics) expects an object that
      // implements getWidth() and getHeight().
      vm.mapSize = {
        getWidth()  { return vm.boothArea.mapGraphic.width;  },
        getHeight() { return vm.boothArea.mapGraphic.height; }
      };

      // This controls the zoom, pan.
      vm.viewingTransformer = new ViewingTransformer(
        vm.boothArea.mapWidth, vm.boothArea.mapHeight);

      updateCalibrationMatrix();
      updateCalibratedVTM();
    }

    // Update the matrix calibrates the booths against the background image.
    function updateCalibrationMatrix() {
      calibrationMatrix = vm.calTransforms
        .reduce((calibrationMatrix, transform) =>
          calibrationMatrix
            .multiply(matrixHelper.cloneMatrix(transform)),
          matrixHelper.createSVGMatrix());
    }

    // Update the calibrated viewing transformation matrix, which is the
    // VTM (pan/zoom) times the calibration matrix.
    function updateCalibratedVTM() {
      vm.calibratedVTM = vm.viewingTransformer
        .getVTM()
        .multiply(calibrationMatrix);
    }

    // Zoom in our out of the map at the mouse's location.
    function zoom(e) {
      const dir    = e.deltaY < 0 ? 1 : -1;
      const origin = {
        x: e.trueOffsetX,
        y: e.trueOffsetY
      };

      vm.viewingTransformer.zoom(30 * dir, null, origin);
      updateCalibratedVTM();
    }

    // Drag the map around.
    function pan(deltaX, deltaY) {
      vm.viewingTransformer.pan(deltaX, deltaY);
      updateCalibratedVTM();
    }

    function drag(e) {
      const delta = panDeltaTracker.getDelta(e);

      if (delta)
        pan(delta.x, delta.y);
    }

    function pinch(e) {
      const origin = {
        x: e.center.x,
        y: e.center.y
      };

      const dir = e.additionalEvent === 'pinchin' ? -1 : 1;

      vm.viewingTransformer.zoom(dir * 10, null, origin);
      updateCalibratedVTM();
    }
  }
})(window.angular);

